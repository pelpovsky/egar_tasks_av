package org.pelpovsky.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pelpovsky.enums.TaskStatus;

import java.time.LocalDateTime;

/**
 * Сущность работы
 *
 * @author Nikiforov Artyom - pelp88@outlook.com
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task {
    private Long id;
    private User user;
    private Field field;
    private String type;
    private LocalDateTime startTime = LocalDateTime.now();
    private LocalDateTime endTime;
    private TaskStatus status;

}
