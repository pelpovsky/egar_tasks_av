package org.pelpovsky.entity;

import lombok.*;
import java.time.LocalDateTime;

/**
 * Сущность погоды
 *
 * @author Gleb Luchinkin
 * @author Artyom Nikiforov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Weather {
    private Long id;
    private Field fieldId;
    private LocalDateTime time;
    private Double airTemperature;
    private Integer humidity;
    private Integer pressure;
    private String description;
    private Integer cloudinessPercent;
    private Double stormPrediction;
    private Double precipitationAmount;
    private String precipitationType;
    private Double phenomenonCode;
    private String iconId;
    private Double windSpeed;
}
