package org.pelpovsky.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Сущность показаний
 *
 * @author Gleb Luchinkin
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Reading {
    private Long id;
    private UUID serialNumber;
    private LocalDateTime time;
    private Double airTemperature;
    private Integer humidity;
    private Integer pressure;
    private Double precipitationAmount;
    private Double windSpeed;
    private Double chemicalLevel;
    private Double manureLevel;
}
