package org.pelpovsky.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pelpovsky.enums.SoilType;

import java.util.UUID;

/**
 * Сущность участка
 *
 * @author Nikiforov Artyom - pelp88@outlook.com
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Field {
    private Long id;
    private Plant plant;
    private SoilType soilType;
    private Double latitude;
    private Double longitude;
    private UUID sensorSerialNumber = UUID.randomUUID();
    private Integer plantHealth = 100;
}