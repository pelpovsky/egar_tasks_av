package org.pelpovsky.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pelpovsky.enums.SoilType;

import java.time.LocalDate;
import java.util.List;

/**
 * Сущность растения
 *
 * @author Gleb Luchinkin
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Plant {
    private Long id;
    private String name;
    private Integer minTemp;
    private Integer maxTemp;
    private List<SoilType> soilType;
    private LocalDate plantingDate;
    private LocalDate ripeningDate;
    private String plantSort;
    private Double manure;
    private Double chemical;
    private Double minHumidity;
    private Double maxHumidity;
}
