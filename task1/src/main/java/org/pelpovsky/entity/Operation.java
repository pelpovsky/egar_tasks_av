package org.pelpovsky.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pelpovsky.enums.RegularOperation;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Operation {
    private Long id;
    private Plant plantId;
    private RegularOperation operationName;
    private LocalDate procDate;
    private Long interval;
}
