package org.pelpovsky.enums;

import lombok.Getter;

@Getter
public enum RegularOperation {
    WATER("Полить растение"),
    CHEMICAL("Добавить химикаты"),
    MANURE("Добавить удобрения"),
    CUT("Подстричь растение");

    private final String name;

    RegularOperation(String name){
        this.name = name;
    }

}
